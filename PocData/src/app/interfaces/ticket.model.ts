export interface Ticket {
    id?: string;
    lastName?: string;
    date?: string;
    heure?: string;
}