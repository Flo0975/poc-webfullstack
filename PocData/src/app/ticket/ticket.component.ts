import { Component, OnInit } from '@angular/core';
import { ListService } from '../list.service';
import { Ticket } from '../interfaces/ticket.model';
import dayGridPlugin from '@fullcalendar/daygrid';

declare var $: any;
@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  calendarPlugins = [dayGridPlugin];
  //appel de l'interface
  tickets: Ticket[];

  constructor(private liste: ListService) { }

  //on met correctement la variable events pour le calendrier
  public events: {
    title: string,
    date: string
  }[] = [];
  ngOnInit() {
    //on recup les rdv
    this.liste.getTickets().subscribe(tickets => {
        this.tickets = tickets;
        console.log(tickets);
        //on boucle pour mettre correctement dans la var events
        for (const t of tickets) {
          this.events.push({
            title: t.lastName,
            date: t.date
          })
          console.log(this.events);
        }
    });
  }
}
