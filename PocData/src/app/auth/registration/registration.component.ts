import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  authError: any;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    //verification error
    this.auth.eventAuthError$.subscribe(data => {
      this.authError = data;
    })
  }
  //récup donnée form et on send au service
  createUser(frm){
    this.auth.createUser(frm.value);
  }

}
