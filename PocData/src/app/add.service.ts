import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Ticket } from './interfaces/ticket.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddService {

  Ticket: Observable<Ticket[]>;
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router) { }

    //on ajoute dans le firestore
  addTicket(lastName: string, date: string, heure: string){
    console.log(lastName, date, heure);
    this.db.collection('rdv').doc(this.db.createId()).set({
      lastName,
      date,
      heure
    });
  }
}
