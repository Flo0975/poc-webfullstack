import { Component, OnInit } from '@angular/core';
import { AddService } from '../add.service';
import { Ticket } from '../interfaces/ticket.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  constructor(private add: AddService, private router: Router) { }

  ngOnInit() {
  }

  //on recup les données du form et les send au service
  addForm(frm){
    this.add.addTicket(frm.value.lastName, frm.value.date, frm.value.heure);
    alert("Ok");
    this.router.navigate(['/ticket']);
  }

}
