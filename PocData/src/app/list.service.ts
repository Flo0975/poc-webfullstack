import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Ticket } from './interfaces/ticket.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  ticketCollection: AngularFirestoreCollection<Ticket>;
  ticket: Observable<Ticket[]>;

  //on recup dans le firestore
  constructor(public db: AngularFirestore){ 
    this.ticket = this.db.collection('rdv').valueChanges();
  }
  getTickets(){
    return this.ticket;
  }
}
