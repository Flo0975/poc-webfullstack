// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBl3PHQtgN9tpAppJGn6yq4xPat0uzNtmg",
    authDomain: "pocwebfullstack.firebaseapp.com",
    databaseURL: "https://pocwebfullstack.firebaseio.com",
    projectId: "pocwebfullstack",
    storageBucket: "pocwebfullstack.appspot.com",
    messagingSenderId: "431785092269",
    appId: "1:431785092269:web:20e79686aaf2a915f49224"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
